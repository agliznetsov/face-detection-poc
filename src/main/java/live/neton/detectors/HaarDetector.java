package live.neton.detectors;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HaarDetector implements ObjectDetector {
	CascadeClassifier cascadeClassifier;

	public HaarDetector(File model) {
		cascadeClassifier = new CascadeClassifier();
		cascadeClassifier.load(model.getAbsolutePath());
	}

	@Override
	public List<Detection> detect(Mat image) {
		long start = System.currentTimeMillis();

		MatOfRect facesDetected = new MatOfRect();
		int minFaceSize = 100;
		cascadeClassifier.detectMultiScale(image,
				facesDetected,
				1.1,
				3,
				Objdetect.CASCADE_SCALE_IMAGE,
				new Size(minFaceSize, minFaceSize),
				new Size()
		);

		Rect[] facesArray = facesDetected.toArray();
		long end = System.currentTimeMillis();
		log.info("Haar took {}ms", (end - start));

		return Arrays.stream(facesArray).map(this::toDetection).collect(Collectors.toList());
	}

	private Detection toDetection(Rect rect) {
		Detection detection = new Detection();
		detection.setP0(rect.tl());
		detection.setP1(rect.br());
		return detection;
	}
}
