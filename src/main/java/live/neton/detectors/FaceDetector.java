package live.neton.detectors;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import org.opencv.imgproc.Imgproc;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FaceDetector implements ObjectDetector {
	Net net;
	double minConfidence;

	public FaceDetector(double confidence) {
		this.minConfidence = confidence;
		net = Dnn.readNetFromCaffe(
				"./src/main/resources/face_detector/deploy.prototxt",
				"./src/main/resources/face_detector/res10_300x300_ssd_iter_140000_fp16.caffemodel");
	}

	@Override
	public List<Detection> detect(Mat loadedImage) {
		long start = System.currentTimeMillis();

		Imgproc.cvtColor(loadedImage, loadedImage, Imgproc.COLOR_RGBA2RGB);

		Mat resizedImage = new Mat();
		Imgproc.resize(loadedImage, resizedImage, new Size(300, 300));
		Mat blob = Dnn.blobFromImage(resizedImage, 1.0, new Size(300, 300), new Scalar(104.0, 177.0, 123.0));

		net.setInput(blob);
		Mat detections = net.forward();
		long end = System.currentTimeMillis();
		log.info("NN took {}ms", (end - start));


		int rows = loadedImage.rows();
		int cols = loadedImage.cols();
		detections = detections.reshape(1, (int)detections.total() / 7);

		List<Detection> res = new ArrayList<>();
		for (int i = 0; i < detections.rows(); ++i) {
			double confidence = detections.get(i, 2)[0];
			if (confidence > minConfidence) {
				int classId = (int) detections.get(i, 1)[0];
				int left = (int) (detections.get(i, 3)[0] * cols);
				int top = (int) (detections.get(i, 4)[0] * rows);
				int right = (int) (detections.get(i, 5)[0] * cols);
				int bottom = (int) (detections.get(i, 6)[0] * rows);

				Detection detection = new Detection();
				detection.setConfidence(confidence);
				detection.setClassId(classId);
				detection.setP0(new Point(left, top));
				detection.setP1(new Point(right, bottom));
				res.add(detection);
			}
		}
		return res;
	}

}
