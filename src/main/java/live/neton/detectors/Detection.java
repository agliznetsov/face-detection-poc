package live.neton.detectors;

import org.opencv.core.Point;

import lombok.Data;

@Data
public class Detection {
	Point p0;
	Point p1;
	Double confidence;
	int classId;
}
