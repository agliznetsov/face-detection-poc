package live.neton.detectors;

import java.util.List;

import org.opencv.core.Mat;

public interface ObjectDetector {
	List<Detection> detect(Mat image);
}
