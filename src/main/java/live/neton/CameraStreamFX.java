//package live.neton;
//
//import javafx.animation.AnimationTimer;
//import javafx.application.Application;
//import javafx.event.ActionEvent;
//import javafx.scene.Scene;
//import javafx.scene.control.Button;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import javafx.scene.layout.HBox;
//import javafx.scene.layout.VBox;
//import javafx.stage.Stage;
//import nu.pattern.OpenCV;
//import org.opencv.core.Mat;
//import org.opencv.core.MatOfByte;
//import org.opencv.core.MatOfRect;
//import org.opencv.core.Rect;
//import org.opencv.core.Scalar;
//import org.opencv.core.Size;
//import org.opencv.imgcodecs.Imgcodecs;
//import org.opencv.imgproc.Imgproc;
//import org.opencv.objdetect.CascadeClassifier;
//import org.opencv.objdetect.Objdetect;
//import org.opencv.videoio.VideoCapture;
//import org.opencv.videoio.VideoWriter;
//
//import java.io.ByteArrayInputStream;
//import java.io.File;
//
//public class CameraStreamFX extends Application {
//
//    public static void main(String[] args) {
//        Application.launch(args);
//    }
//
//    private VideoCapture capture;
//    private VideoWriter videoWriter;
//
//    public void start(Stage stage) throws Exception {
//        OpenCV.loadShared();
//
//        capture = new VideoCapture();
//        capture.open(0);
//
//        new File("./target/filename.avi").delete();
//
//        Button button1 = new Button("Start");
//        Button button2 = new Button("Finish");
//        button1.setOnAction(this::onStart);
//        button2.setOnAction(this::onFinish);
//        ImageView imageView = new ImageView();
//        VBox vbox = new VBox(button1, button2, imageView);
//        Scene scene = new Scene(vbox);
//        stage.setScene(scene);
//        stage.show();
//
//        new AnimationTimer(){
//            @Override
//            public void handle(long l) {
//                imageView.setImage(getCaptureWithFaceDetection());
//            }
//        }.start();
//    }
//
//    private void onStart(ActionEvent actionEvent) {
//        double width = capture.get(3);
//        double height = capture.get(4);
//        videoWriter = new VideoWriter("./target/filename.avi", VideoWriter.fourcc('M','J','P','G'), 25.0, new Size(width, height));
//    }
//
//    private void onFinish(ActionEvent actionEvent) {
//        videoWriter.release();
//        capture.release();
//        System.exit(0);
//    }
//
//    public Image getCapture() {
//        Mat mat = new Mat();
//        capture.read(mat);
//        return mat2Img(mat);
//    }
//
//    public Image getCaptureWithFaceDetection() {
//        Mat mat = new Mat();
//        capture.read(mat);
//        Mat haarClassifiedImg = detectFace(mat);
//        if (videoWriter != null) {
//            videoWriter.write(haarClassifiedImg);
//        }
//        return mat2Img(haarClassifiedImg);
//    }
//
//    public Image mat2Img(Mat mat) {
//        MatOfByte bytes = new MatOfByte();
//        Imgcodecs.imencode(".jpg", mat, bytes);
//        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes.toArray());
//        Image img = new Image(inputStream); return img;
//    }
//
//    public static Mat detectFace(Mat inputImage) {
//        MatOfRect facesDetected = new MatOfRect();
//        CascadeClassifier cascadeClassifier = new CascadeClassifier();
//        int minFaceSize = Math.round(inputImage.rows() * 0.1f);
//        cascadeClassifier.load("./src/main/resources/haarcascades/haarcascade_frontalface_alt.xml");
//        cascadeClassifier.detectMultiScale(inputImage,
//                facesDetected,
//                1.1,
//                3,
//                Objdetect.CASCADE_SCALE_IMAGE,
//                new Size(minFaceSize, minFaceSize),
//                new Size()
//        );
//        Rect[] facesArray =  facesDetected.toArray();
//        for(Rect face : facesArray) {
//            Imgproc.rectangle(inputImage, face.tl(), face.br(), new Scalar(0, 0, 255), 3 );
//        }
//        return inputImage;
//    }
//}