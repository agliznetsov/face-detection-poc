package live.neton;

import java.io.File;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.util.FileSystemUtils;

import live.neton.detectors.Detection;
import live.neton.detectors.FaceDetector;
import live.neton.detectors.HaarDetector;
import live.neton.detectors.ObjectDetector;
import lombok.extern.slf4j.Slf4j;
import nu.pattern.OpenCV;

@Slf4j
public class ImageApp {
    public static void main(String[] args) {
        OpenCV.loadShared();
        new ImageApp().run();
    }

    ObjectDetector faceDetector;
    ObjectDetector haarDetector;

    private void run() {
        haarDetector = new HaarDetector(new File("./src/main/resources/haarcascades/haarcascade_frontalface_alt.xml"));
        faceDetector = new FaceDetector(0.1);

        File sourceRoot = new File("./src/main/resources/images");
        File targetRoot = new File("./target/images");
        FileSystemUtils.deleteRecursively(targetRoot);
        targetRoot.mkdir();
        for (File source : sourceRoot.listFiles()) {
            findFaces(source, new File(targetRoot, source.getName()));
        }
    }

    private void findFaces(File source, File target) {
        Mat loadedImage = Imgcodecs.imread(source.getAbsolutePath());

        findFaces(loadedImage, faceDetector, new Scalar(0, 255, 0));
        findFaces(loadedImage, haarDetector, new Scalar(0, 0, 255));

        Imgcodecs.imwrite(target.getAbsolutePath(), loadedImage);
    }

    private void findFaces(Mat loadedImage, ObjectDetector detector, Scalar color) {
        List<Detection> faces = detector.detect(loadedImage);
        for (Detection face : faces) {
            Imgproc.rectangle(loadedImage, face.getP0(), face.getP1(), color);
            if (face.getConfidence() != null) {
                String label = String.format("%.1f", face.getConfidence());
                Imgproc.putText(loadedImage, label, new Point(face.getP0().x, face.getP0().y + 25),
                        Imgproc.FONT_HERSHEY_SIMPLEX, 1, color, 2);
            }
        }
    }

}
