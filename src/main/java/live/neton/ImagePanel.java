package live.neton;

import java.awt.*;

import javax.swing.*;

public class ImagePanel extends JPanel {

    private Image image;

    public ImagePanel() {
        setPreferredSize(new Dimension(640, 480));
    }

    public void setImage(Image image) {
        this.image = image;
        this.setPreferredSize(new Dimension(image.getWidth(null), image.getHeight(null)));
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (image != null) {
            g.drawImage(image, 0, 0, this); // see javadoc for more info on the parameters
        }
    }

}