package live.neton;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.ByteArrayInputStream;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.*;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;

import live.neton.detectors.Detection;
import live.neton.detectors.FaceDetector;
import live.neton.detectors.HaarDetector;
import live.neton.detectors.ObjectDetector;
import lombok.SneakyThrows;
import nu.pattern.OpenCV;

public class CameraApp extends JFrame {

    public static void main(String[] args) throws Exception {
        OpenCV.loadShared();

        CameraApp cameraStream = new CameraApp();
        cameraStream.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        cameraStream.start();
        cameraStream.pack();
        cameraStream.setVisible(true);
    }

    private VideoCapture capture;
    private VideoWriter videoWriter;
    private ImagePanel imagePanel;
    ObjectDetector faceDetector;
    ObjectDetector haarDetector;

    public void start() {
        haarDetector = new HaarDetector(new File("./src/main/resources/haarcascades/haarcascade_frontalface_alt.xml"));
        faceDetector = new FaceDetector(0.1);
        capture = new VideoCapture();
        capture.open(0);

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        add(panel);

        Button button1 = new Button("Start");
        Button button2 = new Button("Stop");
        button1.addActionListener(this::onStart);
        button2.addActionListener(this::onFinish);
        panel.add(button1);
        panel.add(button2);

        imagePanel = new ImagePanel();
        panel.add(imagePanel);

        Timer timer = new Timer(1000 / 25, this::onFrame);
        timer.setRepeats(true);
        timer.start();
    }

    private void onFrame(ActionEvent actionEvent) {
        imagePanel.setImage(getCaptureWithFaceDetection());
    }

    private void onStart(ActionEvent actionEvent) {
        double width = capture.get(3);
        double height = capture.get(4);
        new File("./target/camera.avi").delete();
        videoWriter = new VideoWriter("./target/camera.avi", VideoWriter.fourcc('M','J','P','G'), 25.0, new Size(width, height));
    }

    private void onFinish(ActionEvent actionEvent) {
        videoWriter.release();
        capture.release();
        System.exit(0);
    }

    private Image getCaptureWithFaceDetection() {
        Mat mat = new Mat();
        capture.read(mat);
        findFaces(mat, faceDetector, new Scalar(0, 255, 0));
        findFaces(mat, haarDetector, new Scalar(0, 0, 255));
        if (videoWriter != null) {
            videoWriter.write(mat);
        }
        return mat2Img(mat);
    }

    @SneakyThrows
    public Image mat2Img(Mat mat) {
        MatOfByte bytes = new MatOfByte();
        Imgcodecs.imencode(".jpg", mat, bytes);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes.toArray());
        Image img = ImageIO.read(inputStream);
        return img;
    }

    private void findFaces(Mat loadedImage, ObjectDetector detector, Scalar color) {
        java.util.List<Detection> faces = detector.detect(loadedImage);
        for (Detection face : faces) {
            Imgproc.rectangle(loadedImage, face.getP0(), face.getP1(), color);
            if (face.getConfidence() != null) {
                String label = String.format("%.1f", face.getConfidence());
                Imgproc.putText(loadedImage, label, new Point(face.getP0().x, face.getP0().y + 25),
                        Imgproc.FONT_HERSHEY_SIMPLEX, 1, color, 2);
            }
        }
    }
}